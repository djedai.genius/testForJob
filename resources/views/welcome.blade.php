<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>All rooms</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <link rel="stylesheet" href="css/datepicker.css">
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
    <div class="container">
        <form method="get"  action="{{url('index')}}">
            <div style="width: 40%; margin-top: 20%;margin-left: 30%" class="well">
                <table  class="table">
                    <thead>
                    <tr>
                        <th >Дата вьезда: <input type="text" class="span2" name="arrivalDate"  data-date-format="dd-mm-yyyy" value="" id="dpd1"></th>
                        <th >Дата выезда: <input type="text" class="span2" name="departureDate" data-date-format="dd-mm-yyyy" value="" id="dpd2"></th>
                    </tr>
                    </thead>
                </table>
                <label for="type">Выберите тип комнаты</label>

                <select  name="type" class="form-control">
                    @foreach($types as $key => $type)
                    <option value="{{$type->id}}" selected>{{ $type->type }}</option>
                    @endforeach
                </select>

                <div style="margin-top: 10%" class="form-group row">
                    <div class="col-md-9"></div>
                    <input type="submit" class="btn btn-primary">
                </div>
            </div>


        </form>
    </div>
    </body>
    <script>
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = jQuery('#dpd1').datepicker({
            onRender: function(date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
            onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');
    </script>
</html>
