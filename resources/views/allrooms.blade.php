<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>All rooms</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <link rel="stylesheet" href="css/datepicker.css">
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <title>Title</title>
</head>
<body>
<div class="container"><h1  style="margin-bottom: 40px">All free the rooms</h1>
    @foreach($rooms as $res)
        <div class="panel panel-default ">
            <div class="panel-body">
                <div class="panel-heading"><h5></h5>
                    @foreach($hotels as $kei => $val)
                        @if ($res->hotel_id == $val->id)
                            <div>Отель: {{ $val->title }}</div>
                        @endif
                    @endforeach
                    @foreach($type as $k => $v)
                        @if($res->type_id == $v->id)
                            <div>Тип выбранных номеров: {{$v->type}}</div>
                        @endif
                    @endforeach
                    <div>Свободных номеров: {{ $res->count}}</div>

                </div>
            </div>
        </div>
    @endforeach
</div>

</body>
</html>