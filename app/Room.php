<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
       'count',  'hotel_id', 'type_id'
    ];
}
