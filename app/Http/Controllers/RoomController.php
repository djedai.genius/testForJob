<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\ReserveRoom;
use App\Room;
use App\TypeRoom;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    public function findFreeRoom (Request $request)
    {
        $type = $request->input('type');
        $arrival = $request->input('arrivalDate');
        $departure = $request->input('departureDate');
        $rooms = Room::where('type_id', '=', $type)->get();
        $hotels = Hotel::All();
        $types = TypeRoom::all();
        $reserved = ReserveRoom::where([['arrival_date','>=', $arrival],['departure_date','<=', $departure],['type_id','=', $type]])->get();
        $free_rooms = [];
         foreach ($rooms as $key => $room) {
                $free_rooms[] = $room;
                foreach ($reserved as $k => $res) {
                    If ($room->hotel_id == $res->hotel_id) {
                        $room->count = ($room->count) - ($res->count);
                    }
                }
            }
            return view('allrooms', ['rooms' => $free_rooms, 'hotels' => $hotels, 'type' => $types]);
    }

    public function index ()
    {
        $types = TypeRoom::All();
        return view('welcome',['types' => $types]);
    }


}
