<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReserveRoom extends Model
{
    protected $fillable = [
      'count', 'hotel_id', 'arrival_date', 'departure_date', 'type_id'
    ];
}
