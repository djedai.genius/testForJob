<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('count');
            $table->integer('hotel_id')->unsigned()->default(0);
            $table->foreign('hotel_id')
                ->references('id')->on('hotels')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('type_id')->unsigned()->default(0);
            $table->foreign('type_id')
                ->references('id')->on('type_rooms')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
