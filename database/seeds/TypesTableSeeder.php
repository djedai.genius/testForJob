<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\TypeRoom::create([
            'type' => 'De Luxe',
        ]);

        \App\TypeRoom::create([
            'type' => 'King Suite',
        ]);
    }
}
