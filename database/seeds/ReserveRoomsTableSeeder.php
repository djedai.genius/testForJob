<?php

use App\ReserveRoom;
use Illuminate\Database\Seeder;

class ReserveRoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        ReserveRoom::create([
            'count' => '10',
            'arrival_date' => '2017-12-12',
            'departure_date' => '2017-12-20',
            'type_id' => '1',
            'hotel_id' => '1'
        ]);

        ReserveRoom::create([
            'count' => '12',
            'arrival_date' => '2017-12-30',
            'departure_date' => '2018-01-9',
            'type_id' => '1',
            'hotel_id' => '2'
        ]);

        ReserveRoom::create([
            'count' => '14',
            'arrival_date' => '2017-12-10',
            'departure_date' => '2017-12-20',
            'type_id' => '2',
            'hotel_id' => '4'
        ]);

        ReserveRoom::create([
            'count' => '10',
            'arrival_date' => '2017-12-17',
            'departure_date' => '2017-12-23',
            'type_id' => '1',
            'hotel_id' => '2'
        ]);

        ReserveRoom::create([
            'count' => '13',
            'arrival_date' => '2017-12-19',
            'departure_date' => '2017-12-26',
            'type_id' => '2',
            'hotel_id' => '3'
        ]);

    }
}
