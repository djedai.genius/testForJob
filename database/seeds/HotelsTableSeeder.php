<?php

use App\Hotel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HotelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Hotel::create([
                'title' => 'Plaza',
            ]);

        Hotel::create([
            'title' => 'Медведица',
        ]);

        Hotel::create([
            'title' => 'Radisson',
        ]);

        Hotel::create([
            'title' => 'Park in'
        ]);

        Hotel::create([
            'title' => 'Atlantis The Palm',
        ]);

    }
}
