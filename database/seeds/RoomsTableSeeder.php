<?php

use App\Room;
use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Room::create([
            'count' => '100',
            'type_id' => '1',
            'hotel_id' => '1'
        ]);

        Room::create([
            'count' => '100',
            'type_id' => '2',
            'hotel_id' => '1'
        ]);

        Room::create([
            'count' => '100',
            'type_id' => '1',
            'hotel_id' => '2'
        ]);

        Room::create([
            'count' => '100',
            'type_id' => '2',
            'hotel_id' => '1'
        ]);

        Room::create([
            'count' => '100',
            'type_id' => '1',
            'hotel_id' => '3'
        ]);

        Room::create([
            'count' => '100',
            'type_id' => '2',
            'hotel_id' => '3'
        ]);

        Room::create([
            'count' => '100',
            'type_id' => '1',
            'hotel_id' => '4'
        ]);

        Room::create([
            'count' => '100',
            'type_id' => '2',
            'hotel_id' => '4'
        ]);

        Room::create([
            'count' => '100',
            'type_id' => '1',
            'hotel_id' => '5'
        ]);

        Room::create([
            'count' => '100',
            'type_id' => '2',
            'hotel_id' => '5'
        ]);
    }
}
